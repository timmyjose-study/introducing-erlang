-module(fact).
-export([factorial/1]).

factorial(N) ->
  factorial(1, N).

factorial(Acc, N)  when N > 0 -> 
  factorial(Acc * N, N - 1);

factorial(Acc, _N) ->
  Acc.
