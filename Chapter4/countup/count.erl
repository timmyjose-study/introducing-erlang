-module(count).
-export([countdown/1, countup/1]).

countdown(From) when From > 0 ->
  io:format("~w!~n", [From]),
  countdown(From - 1);

countdown(0) ->
  io:format("Blastoff!~n").

countup(To) ->
  countup(1, To).

countup(From, To) when From =< To ->
  io:format("~w!~n", [From]),
  countup(From + 1, To);

countup(_From, _To) ->
  io:format("Finished!~n").