-module(fact).
-export([factorial/1]).

factorial(N) ->
  factorial(1, N, 1).

factorial(Curr, N, Acc) when Curr =< N ->
  factorial(Curr + 1, N, Acc * Curr);

factorial(_Curr, _N, Acc) ->
  io:format("Finished.~n"),
  Acc.