-module(throw_demo).
-export([throw_demo/0]).

foo() ->
  throw(my_exception).

throw_demo() ->
  try foo()
  catch
    error:Error -> io:format("Caught an error ~p~n", [Error]);
    throw:Exception -> io:format("Caught an exception ~p~n", [Exception])
  end.
