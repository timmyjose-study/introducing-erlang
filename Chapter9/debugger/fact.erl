-module(fact).
-export([factorial/1]).

factorial(N) when is_number(N) ->
  factorial(N, 1).

factorial(N, Acc) when N >= 0 ->
  factorial(N - 1, Acc * N);

factorial(N, Acc) when N =< 0 ->
  Acc.
