-module(drop).
-export([fall_velocity/2]).

%fall_velocity(Planemo, Distance) ->
%  G = 
%  case Planemo of
%    earth -> 9.9;
%    moon -> 1.6;
%    mars -> 3.71
%  end,
%
%  try math:sqrt(2.0 * G * Distance) of
%    Result -> Result
%  catch
%    error:Error -> {error, Error}
%  end.

fall_velocity(Planemo, Distance) ->
  try 
    G = 
    case Planemo of
      earth -> 9.8;
      moon -> 1.6;
      mars -> 3.71
    end,
    math:sqrt(2.0 * G * Distance)
  of
    Result -> Result
  catch
    error:Error -> {error, Error}
  end.