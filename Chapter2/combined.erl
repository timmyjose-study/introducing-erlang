-module(combined).
-import(drop, [fall_velocity/1]).
-import(convert, [mps_to_mph/1]).
-export([height_to_mph/1]).

%%% convert the given height in meters to mph.
height_to_mph(Meters) -> mps_to_mph(fall_velocity(Meters)).
