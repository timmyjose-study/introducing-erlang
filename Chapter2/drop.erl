%% @author Timmy Jose
%% @doc Functions calculating velocities achieved by objects
%% dropped in a vacuum.
%% @version 0.1
-module(drop).
-export([fall_velocity/1]).

%% @doc Calculates the velocity of an object falling on earth as if
%% it were in a vacuum.

-spec(fall_velocity(number()) -> number()).
fall_velocity(Distance) -> math:sqrt(2.0 * 9.8 * Distance).