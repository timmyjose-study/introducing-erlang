%% @author Timmy Jose
%% @doc Common conversion functions.
%% @version 0.1
-module(convert).
-export([mps_to_mph/1, mps_to_kph/1]).

-spec(mps_to_mph(number()) -> number()).
mps_to_mph(Mps) -> 2.23693629 * Mps.

-spec(mps_to_kph(number()) -> number()).
mps_to_kph(Mps) -> 3.6 * Mps.
