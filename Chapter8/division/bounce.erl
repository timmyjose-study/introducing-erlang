-module(bounce).
-export([report/0]).

report() ->
  receive
    Msg -> io:format("Divided by two ~p~n", [Msg / 2]), report()
  end.