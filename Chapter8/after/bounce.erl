-module(bounce).
-export([report/1]).

report(Count) ->
  receive
    Msg ->
      io:format("Received message #~p: ~p~n", [Count, Msg]),
      report(Count + 1)
  after
    5000 -> {error, timeout}
  end.