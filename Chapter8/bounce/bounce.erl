-module(bounce).
-export([report/0]).

report() ->
  receive
    Msg -> io:format("Received message ~p~n", [Msg])
  end.