-module(bounce).
-export([report/1]).

report(Count) ->
  NewCount =
  receive
    Msg -> io:format("Received message #~p: ~p~n", [Count, Msg]), Count + 1
  end,
  report(NewCount).
