-module(mph_drop).
-export([mph_drop/0]).

mph_drop() ->
  Drop = spawn(drop, drop, []),
  convert(Drop).

convert(Drop) ->
  receive
    {Planemo, Distance} -> % this is received from the client process
      Drop ! {self(), Planemo, Distance},
      convert(Drop);

    {Planemo, Distance, Velocity} -> % this is received from the Drop process
      MphVelocity = 2.23683629 * Velocity,
      io:format("On ~p, a fall of ~p meters yields a velocity of ~p mph.~n",
                [Planemo, Distance, MphVelocity]),
      convert(Drop)
  end.

