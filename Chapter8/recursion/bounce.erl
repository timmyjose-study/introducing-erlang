-module(bounce).
-export([report/0]).

report() ->
  receive 
    Msg -> io:format("Received a message ~p~n", [Msg]), report()
  end.