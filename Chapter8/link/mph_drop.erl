-module(mph_drop).
-export([mph_drop/0]).

mph_drop() ->
  Drop = spawn_link(drop, drop, []),
  convert(Drop).

convert(Drop) ->
  receive 
    {Planemo, Distance} ->
      Drop ! {self(), Planemo, Distance};

    {Planemo, Distance, Velocity} ->
      MphVelocity = 2.23683629 * Velocity,
      io:format("On planet ~p, a distance of ~p meters generates a velocity of ~p mph~n", 
               [Planemo, Distance, MphVelocity])
  end,
  convert(Drop).
