-module(ask).
-export([line/0]).

line() ->
  Planemo = get_planemo(),
  Distance = get_distance(),
  drop:fall_velocity({Planemo, Distance}).

get_planemo() ->
  io:format("Where are you?~n"),
  io:format("  1. Earth~n"),
  io:format("  2. Earths' moon~n"),
  io:format("  3. Mars~n"),
  Char = hd(io:get_line("Which one? > ")),
  char_to_planemo(Char).

char_to_planemo(Char) ->
  if
    [Char] == "1" -> earth;
    Char == $2 -> moon;
    Char == 51 -> mars
  end.

get_distance() ->
  Input = io:get_line("How far in meters? > "),
  Value = string:strip(Input, right, $\n),
  {Distance, _} = string:to_integer(Value),
  Distance.