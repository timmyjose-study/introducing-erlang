-module(listdrop).
-export([falls/1]).

falls(List) -> falls(List, []).

falls([], Acc) -> lists:reverse(Acc);

falls([Term | Terms], Acc) -> falls(Terms, [drop:fall_velocity(Term) | Acc]).
