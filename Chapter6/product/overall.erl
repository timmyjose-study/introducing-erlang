-module(overall).
-export([product/1]).

product([]) -> 0;
product(List) -> product(List, 1).

product([], Acc) -> Acc;
product([Head | Tail], Acc) -> product(Tail, Acc * Head).