-module(map_demo).
-export([new/0, add_entry/3, entries/2, delete_entry/2]).

new() ->
  #{}.

add_entry(Key, Value, Store) ->
  maps:put(Key, Value, Store).

delete_entry(Key, Store) ->
  maps:remove(Key, Store).

entries(Key, Store) ->
  maps:filter(fun (K, _V) -> K == Key end, Store).
  